package pl.akademiakodu.api;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.akademiakodu.model.*;
import pl.akademiakodu.service.CompilerApiService;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@CrossOrigin
@RestController
public class CompilerApiController {

    private CompilerApiService compilerApiService;

    @CrossOrigin
    @GetMapping("/codes/check")
    public String checkCode(@RequestParam String code,
                            @RequestParam String expectedResult) {
        String result = null;
        synchronized (this) {
            compilerApiService = new CompilerApiService(code,
                    expectedResult);
             result = compilerApiService.getResult();
        }
        return result;
    }

}
