package pl.akademiakodu.model;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;


public class SimpleFileGenerator implements FileSourceGenerator {

    private String rootPath;

    public SimpleFileGenerator(String rootPath) {
        this.rootPath = rootPath;
    }

    @Override
    public Path generateJavaFileFromSourceCode(String codeToGenerate) throws IOException {
        String className = getClassName(codeToGenerate);
        List<String> lines = Arrays.asList(codeToGenerate);
        String filePath = rootPath+"/"+className+".java";
        Path file = Paths.get(filePath);
        Files.write(file, lines, Charset.forName("UTF-8"));
        return file;
    }

    private String getClassName(String codeToGenerate) {
        int lengthOfClassWord = 5;
        int lastIndexOfClassWord = codeToGenerate.lastIndexOf("class") + lengthOfClassWord;
        int j = lastIndexOfClassWord;
        while (j < codeToGenerate.length()) {
            if (codeToGenerate.charAt(j) != '{') {
                j++;
            } else {
                break;
            }
        }
        String className = codeToGenerate.substring(lastIndexOfClassWord, j);
        return className.trim();
    }

    public static void main(String[] args) {
        SimpleFileGenerator simpleFileGenerator = new SimpleFileGenerator("/Users/michalos/Desktop");
        try {
            Path path = simpleFileGenerator.generateJavaFileFromSourceCode("" +
                    "public class Hello{\n" +
                    "        public static void main(String[] args){\n" +
                    "System.out.println(\"Hello world\");"+
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "      }\n" +
                    "    }");
            CompilerSimpleExecutor compilerExecutor = new CompilerSimpleExecutor();
            Path compiledClass = compilerExecutor.compileSource(path);
            Path p = compilerExecutor.compileSource(compiledClass);
            try {
                compilerExecutor.runClass(p);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
